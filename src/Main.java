import java.io.File;
import java.util.Scanner;

public class Main {

    //(Configurable) image location
    static final String DIRECTORY = "";
    static final String FILENAME = "img0.txt";

    public static void main(String[] args) {

        int[][] arr = readTextFile(DIRECTORY,FILENAME);

        if(arr == null) {
            return;
        }

        System.out.println("b)");

        printImage(arr);

        System.out.println("c)");

        int[][] arrFiltrado = applyFilter(arr);
        printImage(arrFiltrado);

        System.out.println("d)");

        System.out.println(compareArrays(arr, arrFiltrado) ? "TRUE" : "FALSE");

        //Cumulativo (altera o array)
        arr = arrFiltrado;

        System.out.println("e)");

        printRankOfPart(arr);

        System.out.println("f)");

        printRank(arr);

        System.out.println("g)");

        System.out.println("-- Alteração da cor 1 para a cor 5 --");

        //Cumulativo (altera o array)
        arr = changeValue(arr,1,5);
        printImage(arr);

        System.out.println("h)");

        //Cumulativo (altera o array)
        arr = invert(arr);
        printImage(arr);

        System.out.println("i)");

        System.out.println(getDarkestLine(arr) + 1);

        System.out.println("j)");

        printLowestWith9(arr);

    }

    //Lê o texto de texto

    //Tem as seguintes validações:
    //- Inseridas mais/menos linhas do que o esperado
    //- Inseridos mais/menos caracteres por linha do que o esperado
    //- Inseridos caracteres que não são números

    public static int[][] readTextFile(String directory, String fileName) {
        File homeDir = new File(directory);
        File file = new File(fileName);

        int dimension = 0;

        if(file.exists()) {
            try {
                int[][] x = null; //declaro um array x vazio

                Scanner myReader = new Scanner(file);
                int lineNr = 0;
                while (myReader.hasNextLine()) { // o while percorre enquanto houver mais uma linha para ler
                    String line = myReader.nextLine().trim();

                    //Ignores empty lines
                    if(line.isEmpty())
                        continue;

                    /* Builds the ImageFile object */
                    switch (lineNr) {
                        case 0:
                            break;
                        case 1:
                            if(!isNumeric(line))
                                throw new Exception("A linha relativa à dimensão, não contém um inteiro"); //validação de input

                            dimension = Integer.parseInt(line); //alimentei o módulo setDimension convertendo o valor para int

                            x = new int[dimension][dimension]; //crio um array bidimensional quadrado com o tamanho definido na linha 2 do txt

                            //Marcamos o array com -1's para as verificações finais
                            for(int i = 0; i < dimension; i++) {
                                for (int j = 0; j < dimension; j++) {
                                    x[i][j] = -1;
                                }
                            }

                            break;
                        default:
                            //Valida o comprimento da linha
                            if(dimension != line.length()) //validação de input para o caso de linha maior que a dimensão declarda
                                throw new Exception("A linha " + (lineNr-1) + " não tem a dimensão declarada!");

                            if(lineNr < 2 + dimension) {
                                for (int i = 0; i < dimension; i++) { //preenchimento do array declarado no início do módulo
                                    String nr = line.charAt(i) + ""; //cada linha do txt é considerada uma string única sendo que cada char é um elemento do vetor
                                    // +"" serve para quê? TRANSFORMA O CHAR -> STRING!

                                    if (!isNumeric(nr)) //isNumeric é um módulo que verifica se o caractér é um número
                                        throw new Exception("O elemento (" + (lineNr - 2) + ", " + i + ") não é um número!");

                                    x[lineNr - 2][i] = Integer.parseInt(nr); //populo o array x
                                } //preenche uma linha coluna a coluna e depois passa para a próxima coluna
                            }
                    }
                    lineNr++; //contador do switch
                }

                //Valida o número de linhas
                if(lineNr - 2 != dimension) //validação de input para o caso de nr linhas diferente à dimensão declarada
                    throw new Exception("A imagem que criou não tem a quantidade de linhas que declarou.");

                return x; //passo o array por parâmetro para o módulo setContent que o vai armazenar
            } catch (Exception ex) {
                System.out.println("Houve um problema na leitura do ficheiro:");
                System.out.println("- " + ex.getMessage());
                return null;
            }
        }

        System.out.println("Não conseguimos ler o ficheiro de imagem. Verifique se está no diretório \"" + homeDir.getAbsolutePath() + "\" e se o seu nome é \"" + fileName + "\").");
        return null;
    }

    private static boolean isNumeric(String str) {
        if (str == null) //despista o caso de caractér inexistente
            return false;

        try { //try & catch devolve "false" se o a conversão a integer der erro logo o caractér não é um inteiro
            int i = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }

        return true;
    }

    //Print tool

    public static void printImage(int[][] content){
        for(int i = 0; i < content.length; i++) {
            for(int j = 0; j < content[i].length; j++) {
                System.out.print(content[i][j]);
            }
            System.out.print("\n");
        }
    }

    //Ex C

    public static int[][] applyFilter(int[][] arr){
        int[][] result = new int[arr.length][arr.length];

        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr.length; j++) {
                //Doesn't apply to the borders!
                if(!(i == 0 || j == 0 || i == arr.length - 1 || j == arr.length - 1)) {
                    int n = arr[i - 1][j];
                    int s = arr[i + 1][j];
                    int e = arr[i][j - 1];
                    int w = arr[i][j + 1];

                    int sum = n + s + e + w + arr[i][j];

                    result[i][j] = sum/5;
                } else {
                    result[i][j] = arr[i][j];
                }
            }
        }

        return  result;
    }

    //Ex D

    public static boolean compareArrays(int[][] content, int[][] content2){
        boolean result = true;
        for(int i = 0; i < content.length && result; i++) {
            for(int j = 0; j < content[i].length && result; j++) {
                if(content[i][j] != content2[i][j])
                    result = false;
            }
        }
        return result;
    }

    public static void printRankOfPart(int[][] arr){
        System.out.println(printRankOfPart(arr,2) + "" + printRankOfPart(arr,1));
        System.out.println(printRankOfPart(arr,3) + "" + printRankOfPart(arr,4));
    }

    public static int printRankOfPart(int[][] arr, int part){
        int firstStart = 0;
        int firstEnd = arr.length/2;
        int secondStart = Math.round(arr.length/ (float) 2);
        int secondEnd = arr.length;

        int[] rank = new int[10];
        switch(part) {
            case 1:
                for(int i = firstStart; i < firstEnd; i++) {
                    for(int j = secondStart; j < secondEnd; j++) {
                        rank[arr[i][j]]++;
                    }
                }
                break;
            case 2:
                for(int i = firstStart; i < firstEnd; i++) {
                    for(int j = firstStart; j < firstEnd; j++) {
                        rank[arr[i][j]]++;
                    }
                }
                break;
            case 3:
                for(int i = secondStart; i < secondEnd; i++) {
                    for(int j = firstStart; j < firstEnd; j++) {
                        rank[arr[i][j]]++;
                    }
                }
                break;
            case 4:
                for(int i = secondStart; i < secondEnd; i++) {
                    for(int j = secondStart; j < secondEnd; j++) {
                        rank[arr[i][j]]++;
                    }
                }
                break;
        }

        int max = -1;
        int maxValue = 0;
        for(int i = 0; i < 10; i++) {
            if(rank[i] > maxValue) {
                max = i;
                maxValue = rank[i];
            }
        }
        return max;
    }

    public static void printRank(int[][] arr){
        int[] count = new int[10];
        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr[i].length; j++) {
                count[arr[i][j]]++;
            }
        }

        for (int i = 0; i < count.length ; i++) {
            if(count[i] != 0)
                System.out.print("[" + i + "]");
        }
        System.out.println("");
    }

    public static int[][] changeValue(int[][] arr, int x, int y){
        int[][] result = new int[arr.length][arr.length];

        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr.length; j++) {
                if(arr[i][j] == x) {
                    result[i][j] = y;
                } else {
                    result[i][j] = arr[i][j];
                }
            }
        }
        return  result;
    }

    public static int[][] invert(int[][] arr){
        int[][] result = new int[arr.length][arr.length];

        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr.length/2; j++) {
                int otherSide = arr[i][arr.length-1-j];
                result[i][j] = otherSide;
                result[i][arr.length-1-j] = arr[i][j];
            }
        }
        return  result;
    }

    public static int getDarkestLine(int[][] arr){
        int[] sum = new int[arr.length];

        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr.length; j++) {
                sum[i] += arr[i][j];
            }
        }

        int lowest = 0;
        int lowestValue = sum[0];
        for(int i = 1; i < arr.length; i++) {
            if(sum[i] <= lowestValue) {
                lowestValue = sum[i];
                lowest = i;
            }
        }

        return lowest;
    }

    public static void printLowestWith9(int[][] arr){
        int darkestLine = getDarkestLine(arr);

        for(int j = 0; j < arr.length; j++) {
            arr[darkestLine][j] = 9;
        }

        printImage(arr);
    }


}